export default class User {
    id!: number;
    username!: string;
    created_at!: string;

    constructor(jsonObj: User) {
        this.id = jsonObj.id;
        this.username = jsonObj.username;
        this.created_at = jsonObj.created_at;
    }
}
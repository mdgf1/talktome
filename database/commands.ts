import { PostgrestSingleResponse, createClient } from '@supabase/supabase-js'
import User from '../models/User'

const supabaseUrl = 'https://zgwgblqhmsjlvrrbrfso.supabase.co'
const supabaseKey = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJzdXBhYmFzZSIsInJlZiI6Inpnd2dibHFobXNqbHZycmJyZnNvIiwicm9sZSI6ImFub24iLCJpYXQiOjE2ODI3MTc1MTksImV4cCI6MTk5ODI5MzUxOX0.xuz_s691uFX1vgkvsktWoJIOirDnnoAoOj6YilhcXxg'
const supabase = createClient(supabaseUrl, supabaseKey)

function getUsersList(): Array<User> {
    let users: Array<User> = []
    const query = supabase
    .from('users')
    .select()
    .then((obj) => {
        if (obj.data)
            obj.data.forEach(record => users.push(new User(record)))
    })
    return users
}

export {
    getUsersList
}